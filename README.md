# KAAS

Kode as a service. Building modular pipelines as a team with built-in reproducability.
Kode with a k, because CAAS is already taken and because it is neither edible nor Dutch.

---

### What is this file?
As a team working on a data driven project it is hard to deliver reproducible results.
Ideally data files should not be in any kind of version control, but in a cache at most.
When someone makes a code change in any file, anyone else should be able to click 'run' and get the new results,
even when this file is at the start of the processing pipeline.

Kaas achieves this by defining the processing pipeline in modular pieces called 'sources'.

What about these 'sources'?
A source is defined as follows. A source can:
1. Be requested: For the report we will want to use visualizations. We can request these visualization

2. Be provided: Providing the source, means actually processing x into y (or for some sources 'thin air' into y).

3. Rely on a source: E.G.: A visualization (which is a source itself too) needs the data that is visualized.
This data is the content of another source. The visualization source **relies** on the data source.
When the **'provider'** of the visualization source tells the pipeline that it requires the
data source, the pipeline will find this data source, generate it and provide it to the visualizer on the fly.

---

### Can you give some examples of sources? Sure:
*This example describes an use case (for which this lib actually was (over) developed for: Json files containing tweets
. These files could be downloaded from a server. Build the pipeline to convert the files into a database,
which then could be used to perform analysis on and finally train models on.*

- Files: The json files that contain the tweets can be read from disk or downloaded from a server.
    - Be requested: The pipeline will request this source under the hood when the database is requested,
                        because the database requires these files.
    - Be provided: Some piece of code will have to provide the content of these files to the pipeline. We used the
    ``CollectionProdiver`` wrapper to prioritize the cache over the download script. 
    - Rely on a source: These files do not rely on any other source.


- Database: The database generated from the json files.
    - Be requested: A lot of code will need the contents of the database. These pieces of code will request
                        the database.
    - Be provided: Some piece of code needs to be written that requests the json-files and generates a database
                    from them.
    - Rely on a source: The database is generated from the json-files and therefore relies on them.


- Statistical model: Further along the way, we might want to train some fancy LinearModels on the data.
    - Be requested: You may want to know the learned parameters of the model.
    - Be provided: In the provider step of this ``LinearModel`` source, the model is trained.
    - Rely on a source: To train the model you need the data to train it on. So this source relies e.g. on a 
    'data pre-processing'-source.

---

### So how do I write a source?
You have to write 3 python classes (although they can be combined).

##### The source request
    class RequestForSomeRandomSource(lib.kaas.framework.SourceRequest):
        def __init__(self, some_random_parameter):
            With the sourceRequest other pieces of the code can request this source.
            At a minimum you need to define a **UNIQUE** name.
            Optionally, source can have parameters.
            Just put the following line in your `__init__(self)`:
            `super().__init__(unique_name, {'some_parameters': some_random_parameter})`
    
            # The parameters are optional, so you can leave them out.

##### The provider
    class SomeRandomSourceProvider(lib.kaas.pipeline.DependingSourceProvider): 
        def can_provide(self, request: SourceRequest) -> bool:
            return True if you can provide the request.
    
        def provide(self, request: SourceRequest, sources: Dict[str, Source] = None) -> Source:
            In this method you write the actual code that generates the requested source. The pipeline takes into account
            that this can take a while. In the 'sources'-parameter you will find the same keys as you returned
            in has_dependencies(), but now the values of the dictionary contain the actual sources.
    
        OPTIONAL:
        def has_dependencies(self, request: SourceRequest) -> Dict[str, SourceRequest]:
            This method is optional. Before calling the provide method the pipeline will first check if you need any other
            source to provide the requested source. This method should return a dictionary with REQUESTS that you need in
            provide(). The pipeline will go out and generate the requested sources and provide these when it calls the
            'provide' method.
            
            If you don't want this function:
            replace `lib.kaas.pipeline.DependingSourceProvider` (in the head of the class)
            with    `lib.kaas.framework.SourceProvider`

##### The source
    class SomeRandomSource(lib.kaas.framework.Source):
        ##### Optional
        You don't have to define `__init__()`,
        but this is nice way to pass the data generated by the SourceProvider to the source.
    
        def __init__(self, data: str):
            self._data = data
        #####
    
        def read(self):
            The only method a source has is `read()`. It can return pretty much everything, depending on what kind of source
            This method can be called multiple times, so make sure this method is not computationally intensive. Always put
            the bulk of the work in the `provide()` method of the SourceProvider that provided this source.
    
            Example:
            `return self._data`

---

### In gods name, why did I make it so complicated?
First of, the pre-built classed go a long way to 'just writing the code'.

By using the described structure Kaas can deliver the following benefits (not limited to ;) ):
- Modular code that be written and maintained by specialized team members throughout the entire organization.
- Repeatability, without storing intermediate data in version control.
- The seemingly useless `can_provide()` function enables Kaas to first make an inventory of all the provider that can
satisfy a ``SourceRequest`` after which it can make an optimal solution.
- Before execution, Kaas generates a execution graph to detect circular dependencies and duplicate requests. If the same
source is requested multiple times, it is generated only once.
