from typing import List, Union

from ..framework import SourceRequest, SourceProvider, Source


class CollectionProvider(SourceProvider):
    def __init__(self, source_providers: List[SourceProvider], choosing_mode: str = 'only_option'):
        """
        :param source_providers: List of source providers
        :param choosing_mode: 'first_option' or 'only_option', choosing behaviour if multiple providers can provider a
        request
        """
        self._source_providers = source_providers
        self.choosing_mode = choosing_mode
        super().__init__()

    def can_provide(self, request: SourceRequest) -> bool:
        """
        Check if source can be provided by any source provider
        :param request: Source request
        :return: if the source can be provided
        """
        return self._get_provider(request) is not None

    def provide(self, request: SourceRequest, **kwargs) -> Source:
        """
        Provide source
        :param request: Source request
        :return: source
        """
        return self._get_provider(request).provide(request, **kwargs)

    def _get_provider(self, request: SourceRequest) -> SourceProvider:
        """
        Get the provider that can provide the requested source
        :param request: Source request
        :return: Source provider that can provide the requested source, None if source cannot be provided
        """

        # Build a list of possible providers
        possible_providers = []

        # Loop providers
        for provider in self._source_providers:
            # Check if provider can provide source
            if provider.can_provide(request):
                # Add provider to list
                possible_providers.append(provider)

        # Check if deterministic decision can be made for the provider
        return self._choose_provider(possible_providers)

    def _choose_provider(self, possible_providers: List[SourceProvider]) -> Union[SourceProvider, None]:
        """
        Choose the provider.
        :param possible_providers: List of providers to choose from
        :return: selected source provider, None if possible_providers is an empty list
        :raise NotDeterministicError: if no decision could be made
        """
        if self.choosing_mode == 'first_option':
            return self._choose_provider_first_option(possible_providers)
        elif self.choosing_mode == 'only_option':
            return self._choose_provider_only_option(possible_providers)

        raise ValueError('Invalid choose mode')

    @staticmethod
    def _choose_provider_first_option(possible_providers: List[SourceProvider]) -> Union[SourceProvider, None]:
        if len(possible_providers) == 0:
            return None

        return possible_providers[0]

    @staticmethod
    def _choose_provider_only_option(possible_providers: List[SourceProvider]) -> Union[SourceProvider, None]:
        # Check if any provider is available
        if len(possible_providers) == 0:
            return None

        # Check if only one provider is available
        if len(possible_providers) == 1:
            return possible_providers[0]

        # No decision could be made
        raise NotDeterministicError('Cannot deterministically choose from possible sources')


class NotDeterministicError(Exception):
    def __init__(self, message: str):
        self.message = message
        super().__init__()

    def __str__(self):
        return self.message
