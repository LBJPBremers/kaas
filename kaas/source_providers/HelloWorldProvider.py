import time

from ..framework import SourceRequest, SourceProvider, Source
from ..sources import StringSource


class HelloWorldProvider(SourceProvider):
    hash = 'hello_world_hash'

    def can_provide(self, request: SourceRequest) -> bool:
        return request.source_hash is self.hash

    def provide(self, request: SourceRequest, **kwargs) -> Source:
        name = request.parameters.get('name')
        content = 'Hello {}'.format(name)
        return StringSource(content)


class VerySlowHelloWorldProvider(HelloWorldProvider):
    def provide(self, request: SourceRequest, **kwargs) -> Source:
        time.sleep(2)
        return super().provide(request, **kwargs)


class HelloWorldRequest(SourceRequest):
    def __init__(self, name: str = 'World'):
        super().__init__(HelloWorldProvider.hash, {'name': name})
