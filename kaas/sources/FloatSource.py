from ..framework import Source


class FloatSource(Source):
    def __init__(self, value: float):
        self._value = value

    def read(self):
        return self._value
