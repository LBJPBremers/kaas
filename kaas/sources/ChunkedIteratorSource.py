import collections
from abc import ABC, abstractmethod
from typing import Any, Iterable

from ..framework import Source


class ChunkGetter(ABC):
    @abstractmethod
    def get_chunk(self, chunk_id: Any) -> Any:
        pass


class ChunkedIteratorSource(Source):
    def __init__(self, chunk_getter: ChunkGetter, chunk_ids: Iterable[Any]):
        self._chunk_getter = chunk_getter
        self._chunk_ids = chunk_ids

    def read(self, start_index=0) -> Iterable:
        for i, chunk_id in enumerate(self._chunk_ids):
            if i < start_index:
                continue
            yield self._chunk_getter.get_chunk(chunk_id)

    def __len__(self):
        if isinstance(self._chunk_ids, collections.Sized):
            return len(self._chunk_ids)
        else:
            raise TypeError('Chunked iterator has no predetermined length')
