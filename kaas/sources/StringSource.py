from ..framework import Source


class StringSource(Source):
    def __init__(self, content: str):
        self._content = content

    def read(self):
        return self._content
