from ..framework import Source


class IntSource(Source):
    def __init__(self, value: int):
        self._value = value

    def read(self):
        return self._value
