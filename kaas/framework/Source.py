from abc import ABC, abstractmethod


class Source(ABC):
    @abstractmethod
    def read(self):
        """
        Provide the actual data
        :return: source data
        """
