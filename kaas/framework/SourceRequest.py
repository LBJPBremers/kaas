from typing import Dict

from deepdiff import DeepDiff


class SourceRequest:
    def __init__(self, source_hash: str, parameters: Dict = None):
        if parameters is None:
            parameters = {}
        self.source_hash = source_hash
        self.parameters = parameters

    def __str__(self):
        return 'Hash: {}, parameters: {}'.format(self.source_hash, self.parameters)

    def __eq__(self, other):
        # Compare source hash
        if self.source_hash is not other.source_hash:
            return False

        # Compare parameters
        diff = DeepDiff(self.parameters, other.parameters, report_repetition=True)
        if len(diff) > 0:
            return False

        return True
