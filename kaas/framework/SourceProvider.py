from abc import ABC, abstractmethod

from .Source import Source
from .SourceRequest import SourceRequest


class SourceProvider(ABC):
    @abstractmethod
    def can_provide(self, request: SourceRequest) -> bool:
        """
        Checks if this source provider can provider the requested source.
        :param request: Source request to check availability for
        :return: if this source provider can provider the requested source
        """
        pass

    @abstractmethod
    def provide(self, request: SourceRequest, **kwargs) -> Source:
        """
        Provide the source. This may involve the generation of the source
        :param request: source request
        :return: the source
        """
        pass
