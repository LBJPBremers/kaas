from abc import abstractmethod
from typing import Dict

from ..framework import SourceProvider, SourceRequest, Source


class DependingSourceProvider(SourceProvider):
    """
    Source with dependencies
    """

    sources_kwarg = 'sources'

    @abstractmethod
    def has_dependencies(self, request: SourceRequest) -> Dict[str, SourceRequest]:
        pass

    @abstractmethod
    def provide(self, request: SourceRequest, sources: Dict[str, Source] = None) -> Source:
        pass


class NoDependencyProviderError(Exception):
    def __init__(self, request: SourceRequest):
        self.request = request
        super().__init__()

    def __str__(self):
        return 'No dependency provider provided when providing request: {}'.format(str(self.request))
