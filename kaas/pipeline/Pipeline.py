from typing import List

from .DependencyNode import DependencyNode
from .DependingSourceProvider import DependingSourceProvider
from .schedulers import Scheduler
from ..framework import SourceRequest, Source, SourceProvider
from ..source_providers import CollectionProvider


class Pipeline(CollectionProvider, DependingSourceProvider):
    def __init__(self, source_providers: List[SourceProvider], scheduler: Scheduler):
        self._scheduler = scheduler
        super().__init__(source_providers)

    def has_dependencies(self, request: SourceRequest) -> List[SourceRequest]:
        raise NotImplementedError

    def can_provide(self, request: SourceRequest) -> bool:
        """
        Check if source and it's dependencies can be provided
        :param request: Source request
        :return: if the source can be provided
        """
        # Build dependency tree
        root_node = self.build_dependency_request_tree(request)
        return root_node.tree_can_be_provided()

    def provide(self, request: SourceRequest, **kwargs) -> Source:
        # Build dependency tree
        dep_tree = self.build_dependency_request_tree(request)

        # Fail if tree can not be provided
        if not dep_tree.tree_can_be_provided():
            raise UnmetSourceDependency(dep_tree)

            # Let the scheduler handle the resolving of the request
        return self._scheduler.run(super(), dep_tree)

    def build_dependency_request_tree(self, request: SourceRequest,
                                      _parent_node: DependencyNode = None) -> DependencyNode:
        """
        Build a dependency tree that satisfies the request
        :param request: Source Request
        :param _parent_node: Parent node for circular dependency check
        :return: Dependency Tree node
        """

        # Get underlying source provider for request
        provider = self._get_provider(request)

        # Check if circular dependency (if has parent)
        if _parent_node is not None:
            is_circular = _parent_node.branch_includes_request(request)
        else:
            is_circular = False

        # Create node based on request
        node = DependencyNode(request, can_be_provided=provider is not None, is_circular=is_circular,
                              parent_node=_parent_node)

        # Node if leaf if request cannot not be provided or is circular
        if not node.tree_can_be_provided() or is_circular:
            return node

        # Check if request has dependencies
        if isinstance(provider, DependingSourceProvider):
            # Build dependency requests
            dependency_requests = provider.has_dependencies(request)

            # Build child nodes for dependencies
            for dep_name, dep_request in dependency_requests.items():
                # Build dependency tree for child node
                child_node = self.build_dependency_request_tree(dep_request, node)

                # Add child node to current node
                node.add_child_node(dep_name, child_node)

        # Check for cycles (top node only)
        if _parent_node is None:
            cycles = node.tree_has_cycles()
            if cycles is not None:
                raise CircularDependenciesError(node, cycles)

        return node

    def _topological_provide(self, request: SourceRequest):
        pass


class UnmetSourceDependency(Exception):
    def __init__(self, dependency_tree: DependencyNode):
        self.dependency_tree = dependency_tree
        super().__init__()

    def __str__(self):
        return 'Cannot provide this request, because some dependency can not be provided:\n{}'.format(
            self.dependency_tree.tree_str())


class CircularDependenciesError(Exception):
    def __init__(self, root_node: DependencyNode, cycle_paths: List[str]):
        """
        :param root_node: Root node of the tree with cycles
        :param cycle_paths: dot-separated paths to the cycles
        """
        self.root_node = root_node
        self.cycle_paths = cycle_paths
        super().__init__()

    def __str__(self):
        # Build tree str
        output = 'DEPENDENCY TREE:\n' + self.root_node.tree_str()

        # Add cycle paths
        output += '\nCIRCULAR DEPENDENCY PATHS:'
        for cycle_path in self.cycle_paths:
            output += '\n- {}'.format(cycle_path)

        return 'Circular dependencies.\n{}'.format(output)
