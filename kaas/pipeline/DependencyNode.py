from typing import List, Union

from ..framework import SourceRequest


class DependencyNode:
    def __init__(self, request: SourceRequest, can_be_provided: bool = True, is_circular: bool = False,
                 parent_node=None):
        self.request = request
        self.can_be_provided = can_be_provided
        self.children = {}
        self.is_circular = is_circular
        self._parent = parent_node

    def add_child_node(self, name, child_node):
        """
        Add node as child node
        :param name: Name of the child
        :param child_node: Node to add as child
        """
        self.children[name] = child_node

    def tree_str(self, _str: str = '', _prefix: str = '', _last: bool = True, _name: str = '') -> str:
        # Print current node
        pointer = '`-' if _last else '|-'
        _name = '[{}]: '.format(_name) if _name != '' else ''
        _str = _prefix + pointer + _name + str(self) + '\n'

        # Print children
        i_last_child = len(self.children) - 1
        _prefix += '    ' if _last else '|  '
        for i, (name, child) in enumerate(self.children.items()):
            _last = i == i_last_child
            _str += child.tree_str(_str, _prefix, _last, name)

        return _str

    def tree_can_be_provided(self) -> bool:
        # Check if self can be provided
        if not self.can_be_provided:
            return False

        # Check if any child can not be provided
        for child in self.children.values():
            if not child.tree_can_be_provided():
                return False

        return True

    def tree_has_cycles(self) -> Union[List[str], None]:
        """
        :return: list of dot-separated paths to circular dependencies or None if there is no cycle
        """
        cycles = []

        # Check if self is circular
        if self.is_circular:
            cycles.append('')

        # Check for circular child
        for name, child in self.children.items():
            # Poll child for cycles
            child_cycles = child.tree_has_cycles()

            # Append cycles if there are any
            if child_cycles is not None:
                for cycle_path in child_cycles:
                    if cycle_path != '':
                        name += '.' + cycle_path
                    cycles.append(name)

        # Return None is there are no cycles
        if len(cycles) == 0:
            return None

        return cycles

    def get_child_at_path(self, path: str):
        """
        :param path: dot-separated path of child names
        :return: Found DependencyNode
        """

        # Get first part of path
        path_parts = path.split('.', 1)
        if len(path_parts) == 1:
            # Reached end of path
            return self

        return self.children[path_parts[1]].get_child_at_path()

    def branch_includes_request(self, request: SourceRequest):
        """
        Checks if any node in the upstream branch (inc. self) includes request
        :param request: Source request to check for
        :return: if the branch includes the request
        """
        # Check if self
        if request == self.request:
            return True

        # Check upstream branch (if parent is set)
        if self._parent:
            return self._parent.branch_includes_request(request)

        return False

    def get_root(self):
        """
        Get the root node of the tree this node is a part of
        :return: Root node (DependencyNode)
        """
        # Return self, if there is no parent
        if self._parent is None:
            return self

        # Return node of parent
        return self._parent.get_root()

    def flatten(self) -> List:
        """
        :return: List of self and all child nodes
        """
        nodes = [self]

        # Add children
        for child in self.children.values():
            nodes += child.flatten()

        return nodes

    def __str__(self):
        # Build list of errors
        errors = []
        if not self.can_be_provided:
            errors.append('COULD_NOT_PROVIDE')

        if self.is_circular:
            errors.append('CIRCULAR_DEP')

        # Convert request to string
        request_str = str(self.request)

        if len(errors) == 0:
            return request_str
        else:
            # Build error prefix
            err_prefix = '({}):'.format(', '.join(errors))
            return err_prefix + request_str
