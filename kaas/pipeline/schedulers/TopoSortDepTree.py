"""
Remember that course strait from everyone's nightmare, Foundation of Computing?
Turns out that it has applications.

For example, this file uses graph theory to build a DAG from the dependency tree and perform a topological sort on it.
It also removes duplicate dependencies so that they can be reused.
With this topological sort we can find to optimal order of resolving for the dependency tree.

Just use the function that is on the bottom of the file.
"""
from typing import List, Dict, Union, Set

from kaas.framework import SourceRequest
from kaas.pipeline.DependencyNode import DependencyNode


class DependencyDAGNode:
    def __init__(self, index: int, tree_node: DependencyNode):
        self.index = index
        self.tree_node = tree_node
        self._incoming_edges = {}  # Dependencies {index: source_name}
        self._outgoing_edges = {}  # Nodes that depend on this node {index: source_name}

    def add_incoming_edge(self, index: int, source_name: str):
        self._incoming_edges[index] = source_name

    def add_outgoing_edge(self, index: int, source_name: str):
        self._outgoing_edges[index] = source_name

    def incoming_edges(self, skip_indexes=None) -> Dict[int, str]:
        """
        Incoming edges point to the dependencies of this node
        :param skip_indexes: Set of indexes to filter out result
        :return: List of indexes of incoming nodes, except for skip_indexes
        """
        if skip_indexes is None:
            skip_indexes = set()

        return {
            index: source_name
            for index, source_name in self._incoming_edges.items()
            if index not in skip_indexes  # Filter out skip_indexes
        }

    def outgoing_edges(self, skip_indexes=None):
        """
        Outgoing edges point to the nodes that dependent on this node
        :param skip_indexes: Set of indexes to filter out result
        :return: List of indexes of outgoing nodes, except for skip_indexes
        """
        if skip_indexes is None:
            skip_indexes = set()

        return {
            index: source_name
            for index, source_name in self._outgoing_edges.items()
            if index not in skip_indexes  # Filter out skip_indexes
        }


class DAG:
    def __init__(self, flat_tree: List[DependencyNode]):
        self.nodes: Dict[int, DependencyDAGNode] = {}
        self._last_index: int = 0
        self._from_flat_tree(flat_tree)

    def topological_sort(self) -> List[DependencyDAGNode]:
        """
        :return: A topological order of the DAG
        """
        topo_sorted: List[DependencyDAGNode] = []

        processed_nodes = set()
        sources = self.source_node_indexes(processed_nodes)
        while len(sources) > 0:
            # Take source from sources
            source_index = sources.pop()
            source = self.nodes[source_index]

            # Add source to end of output
            topo_sorted.append(source)

            # Mark source as processed
            processed_nodes.add(source_index)

            # Check if any depending node because source
            for node_index in source.outgoing_edges().keys():
                if len(self.nodes[node_index].incoming_edges(processed_nodes)) == 0:
                    sources.add(node_index)

        # Check for cycle
        if len([index for index in self.nodes.keys() if index not in processed_nodes]) != 0:
            raise ValueError('Graph has at least one cycle')

        return topo_sorted

    def add_node(self, node: DependencyDAGNode):
        self.nodes[node.index] = node

    def remove_node(self, index):
        del self.nodes[index]

    def source_node_indexes(self, skip_indexes: Set[int]) -> Set[int]:
        """
        :param skip_indexes: Indexes to filter out
        :return: List of nodes with no incoming edges
        """
        return {index for index, node in self.nodes.items() if len(node.incoming_edges(skip_indexes)) == 0}

    def _has_request(self, request: SourceRequest) -> Union[None, int]:
        """
        Check if the DAG already has a node that holds the given request
        :param request: Source request to search for
        :return: index of the node or None if not found
        """
        for index, node in self.nodes.items():
            if node.tree_node.request == request:
                return index
        return None

    def _from_flat_tree(self, flat_tree: List[DependencyNode]):
        # Populate nodes
        for tree_node in flat_tree:
            # Check if request is already present on graph
            index = self._has_request(tree_node.request)

            # Add new node to graph is not already added
            if index is None:
                # Build dag node
                self._last_index += 1
                dag_node = DependencyDAGNode(self._last_index, tree_node)

                # Add note to the DAG
                self.add_node(dag_node)

        # Populate edges
        for index, dag_node in self.nodes.items():
            # Loop through dependencies
            for source_name, dep in dag_node.tree_node.children.items():
                # Find index of dependency node as incoming edge
                dep_index = self._has_request(dep.request)
                dag_node.add_incoming_edge(dep_index, source_name)

                # Add as outgoing edge to dependency node
                self.nodes[dep_index].add_outgoing_edge(index, source_name)


def topological_sort_dep_tree(root_node: DependencyNode) -> List[DependencyDAGNode]:
    """
    Convert dependency tree to DAG and sort topological
    :param root_node: Root node of the dependency tree
    :return:
    """

    # Flatten tree
    flat_tree = root_node.flatten()

    # Convert to DAG
    dag = DAG(flat_tree)

    # Topological sort
    topo_sorted = dag.topological_sort()

    return topo_sorted
