from abc import ABC, abstractmethod

from kaas.framework import SourceProvider, Source
from kaas.pipeline.DependencyNode import DependencyNode


class Scheduler(ABC):

    @abstractmethod
    def run(self, provider: SourceProvider, node: DependencyNode) -> Source:
        """
        Run scheduler
        :param provider: SourceProvider that can provide sources for the requests in the
        :param node: Node to provide the source for
        :return:
        """
        pass
