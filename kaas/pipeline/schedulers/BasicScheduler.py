from typing import Dict

from .Scheduler import Scheduler
from ..DependencyNode import DependencyNode
from ...framework import SourceProvider, Source


class BasicScheduler(Scheduler):
    """
    Top down (naive) approach
    - no reuse of dependencies
    - recursive
    - duplicate dependencies are generated multiple times
    """

    def run(self, provider: SourceProvider, node: DependencyNode) -> Source:
        # Provides sources
        sources = self._provide_node_sources(provider, node)

        # Provide root request
        return provider.provide(node.request, sources=sources)

    def _provide_node_sources(self, provider: SourceProvider, node: DependencyNode) -> Dict[str, Source]:
        sources = {}
        for name, child in node.children.items():
            sources[name] = self.run(provider, child)

        return sources
