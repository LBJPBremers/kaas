from typing import Dict

from .Scheduler import Scheduler
from .TopoSortDepTree import topological_sort_dep_tree
from ..DependencyNode import DependencyNode
from ..DependingSourceProvider import DependingSourceProvider
from ...framework import SourceProvider, Source


class BasicTopoScheduler(Scheduler):
    """
    Basic topologically sorted schedulers
    - Iterative
    - Reuse of dependencies
    - Linearly executed, no parallel execution
    """

    def run(self, provider: SourceProvider, node: DependencyNode) -> Source:
        # TopoSort dependencies
        topo_sorted_tree = topological_sort_dep_tree(node)

        # Execute TopoSorted tree
        sources: Dict[int, Source] = {}  # key = node index
        for dag_node in topo_sorted_tree:
            # Select sources for node
            node_sources: Dict[str, Source] = {}
            for dep_node_index, source_name in dag_node.incoming_edges().items():
                node_sources[source_name] = sources[dep_node_index]

            # Provide node
            kwargs = {
                DependingSourceProvider.sources_kwarg: node_sources
            }
            source = provider.provide(dag_node.tree_node.request, **kwargs)

            # Register source
            sources[dag_node.index] = source

        # Return last source
        last_dag_node_index = topo_sorted_tree[-1].index
        return sources[last_dag_node_index]
