import kaas

# Define the pipeline (use one pipeline for the entire project)
pipeline = kaas.pipeline.Pipeline([
    kaas.source_providers.HelloWorldProvider()
], kaas.pipeline.schedulers.BasicTopoScheduler())

# Define the request
request = kaas.source_providers.HelloWorldRequest(name='Laurens')

# Let the pipeline figure out the rest
requested_source = pipeline.provide(request)

# Read the source
print(requested_source.read())
