from typing import Iterable, Any

import kaas
from kaas.framework import SourceRequest, Source

"""
Sometimes you want to say hello to someone with a very long name.
In that case dependencies might want to chunk-read this message.
Let us discover how this can be done with Kaas.

(The use-case for this is of course large files or collections of files)
"""


class ChunkedHelloWorldProvider(kaas.source_providers.HelloWorldProvider, kaas.sources.ChunkGetter):

    def provide(self, request: SourceRequest, **kwargs) -> Source:
        # Instead of returning a StringSource, we return a chunked source
        # Each char of message will be its own chunk
        name = request.parameters.get('name')
        message = 'Hello {}'.format(name)
        return kaas.sources.ChunkedIteratorSource(self, self.split_message(message))

    def split_message(self, message) -> Iterable:
        # Yields is used here to illustrate that the list of chunk_ids does not have to be determined beforehand.
        for char in message:
            yield char

    def get_chunk(self, chunk_id: Any) -> Any:
        # Usually in this method we read a file or part of a file indexed by the 'chunk_id',
        # But in this lazy example, the chunk_id holds the actual data
        return chunk_id


# Define the pipeline (use one pipeline for the entire project)
pipeline = kaas.pipeline.Pipeline([
    ChunkedHelloWorldProvider()
], kaas.pipeline.schedulers.BasicTopoScheduler())

# Define the request
request = kaas.source_providers.HelloWorldRequest(name='Laurens')

# Let the pipeline figure out the rest
requested_source = pipeline.provide(request)

# Read the source chunk by chunk
for i, chunk in enumerate(requested_source.read()):
    print(i, chunk)
