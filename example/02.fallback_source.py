import kaas

# Define the pipeline (use one pipeline for the entire project)
pipeline = kaas.pipeline.Pipeline([
    # This is how you define a source that can be read from cache or generated.
    kaas.source_providers.CollectionProvider([
        # Put the cached source on top
        kaas.source_providers.HelloWorldProvider(),
        # Put the 'slow' process second so it is only used when the first provider is not available
        kaas.source_providers.VerySlowHelloWorldProvider()
        # Set the choosing_mode to 'first_option' to prioritize the first provider of the second one.
    ], choosing_mode='first_option')
], kaas.pipeline.schedulers.BasicTopoScheduler())

# Define the request
request = kaas.source_providers.HelloWorldRequest(name='Laurens')

# Let the pipeline figure out the rest
requested_source = pipeline.provide(request)

# Read the source
print(requested_source.read())
