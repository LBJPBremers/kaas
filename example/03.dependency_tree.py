from typing import Dict

import kaas
from kaas.framework import SourceRequest, Source


# Let's write a small source that counts the size of a hello worlds message.
class HelloWorldSizeProvider(kaas.pipeline.DependingSourceProvider):
    # This hash is used to match the request to the provider
    # It is a arbitrary code construct that was developed during the usage of Kaas
    hash = 'hello_world_sizer'

    def has_dependencies(self, request: SourceRequest) -> Dict[str, SourceRequest]:
        # This source depends on the HelloWorldProvider
        return {
            'message': kaas.source_providers.HelloWorldRequest(name=request.parameters.get('name'))
        }

    def provide(self, request: SourceRequest, sources: Dict[str, Source] = None) -> Source:
        # Read the message
        message = sources.get('message').read()

        # Calculate the length of the message
        size = len(message)

        # Return the value as a source
        return kaas.sources.IntSource(size)

    def can_provide(self, request: SourceRequest) -> bool:
        return request.source_hash is self.hash


# And we define a request to request this new size-source
class HelloWorldSizeRequest(kaas.framework.SourceRequest):
    def __init__(self, name):
        super().__init__(HelloWorldSizeProvider.hash, {'name': name})

    def __str__(self):
        """
        Fully optional.
        For pretty print in the dependency tree.
        """
        return 'Size of helloWorld message for name "{}"'.format(self.parameters.get('name'))


# Define the pipeline (use one pipeline for the entire project)
pipeline = kaas.pipeline.Pipeline([
    # Order does not matter
    kaas.source_providers.HelloWorldProvider(),
    HelloWorldSizeProvider()
], kaas.pipeline.schedulers.BasicTopoScheduler())

# Define the request
request = HelloWorldSizeRequest(name='Laurens')

# Print the dependency tree
print(pipeline.build_dependency_request_tree(request).tree_str())

# Let the pipeline figure out the rest
requested_source = pipeline.provide(request)

# Read the source
print('Message length:', requested_source.read())
